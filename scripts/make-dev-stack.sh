#!/bin/bash

set -e

discovery_token=$(curl -s https://discovery.etcd.io/new?size=3)
stack_name="ta-dev-coreos-demo"
template_file="file://$(pwd)/coreos-3-worker.json"
params_file="file://$(pwd)/aws-demo-params.json"
tags_file="file://$(pwd)/aws-demo-tags.json"

# osx 
sed -i '' -e '/DiscoveryURL/ {' -e 'n; s|.*|        \"ParameterValue\": \"'"${discovery_token}"'"|' -e '}' aws-demo-params.json

aws cloudformation validate-template --template-body ${template_file}

echo "aws cloudformation create-stack --stackname ${stack_name} --template-body ${template_file} --parameters ${params_file} --tags ${tags_file}"
aws cloudformation create-stack --stack-name ${stack_name} --template-body ${template_file} --parameters ${params_file} --tags ${tags_file}

#echo "aws cloudformation update-stack --stack-name ${stack_name} --template-body ${template_file} --parameters ${params_file} --tags ${tags_file}"
#aws cloudformation update-stack --stack-name ${stack_name} --template-body ${template_file} --parameters ${params_file} --tags ${tags_file}

# watch aws cloudformation  describe-stack-events --stack-name ta-dev-coreos-demo 
