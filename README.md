# aws lunch and learn
This repo contains the information from the lunch and learn about AWS on Wed, Sept 7 2016.

# running
You can use the [make-dev-stack.sh](scripts/make-dev-stack.sh) script to spin up an environment.  It has been modified to append your username to the end of the stack when making it.  This script only works on MacOSX and probably Linux at this time.


```
# simply open up your favorite terminal and run
./scripts/make-dev-stack.sh
```

# modifying things
The [template](template/coreos-3-worker.json) from the presentation is all set to create you an ELB with security groups and an ASG running [CoreOS](https://coreos.com/).  You can feel free to fork and play around wtih this as much as you want.  The [params](variables/aws-demo-params.json) are used to pass parameters into the cloudformation cli.  The [tags](varibales/aws-demo-tags.json) are appended to the created resources in AWS.

# presentation
[Slides](https://bitbucket.org/taelliot/aws-lunch-and-learn/raw/master/presentation/WTF_is_AWS.pptx) will be updated as needed.
