{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "CoreOS on EC2: http://coreos.com/docs/running-coreos/cloud-providers/ec2/",
    "Mappings": {
        "RegionMap": {
            "eu-central-1": {
                "AMI": "ami-c90bf8a6"
            },
            "ap-northeast-1": {
                "AMI": "ami-85e530e4"
            },
            "us-gov-west-1": {
                "AMI": "ami-e6932a87"
            },
            "ap-northeast-2": {
                "AMI": "ami-9014c1fe"
            },
            "ap-south-1": {
                "AMI": "ami-df98edb0"
            },
            "sa-east-1": {
                "AMI": "ami-eb27b687"
            },
            "ap-southeast-2": {
                "AMI": "ami-a86051cb"
            },
            "ap-southeast-1": {
                "AMI": "ami-5a26fd39"
            },
            "us-east-1": {
                "AMI": "ami-1c94e10b"
            },
            "us-west-2": {
                "AMI": "ami-06af7f66"
            },
            "us-west-1": {
                "AMI": "ami-43561a23"
            },
            "eu-west-1": {
                "AMI": "ami-e3d6ab90"
            }
        }
    },
    "Parameters": {
        "InstanceType": {
            "Description": "EC2 HVM instance type (m3.medium, etc).",
            "Type": "String",
            "Default": "t2.micro",
            "ConstraintDescription": "Must be a valid EC2 HVM instance type."
        },
        "ClusterSize": {
            "Default": "3",
            "MinValue": "3",
            "MaxValue": "12",
            "Description": "Number of nodes in cluster (3-12).",
            "Type": "Number"
        },
        "DiscoveryURL": {
            "Description": "An unique etcd cluster discovery URL. Grab a new token from https://discovery.etcd.io/new?size=<your cluster size>",
            "Type": "String"
        },
        "MachineMetadata": {
            "Description": "A list of key value entries to add to the machine metadata (provider=aws,instance=t2.micro,region=us-east)",
            "Type": "String"
        },
        "AdvertisedIPAddress": {
            "Description": "Use 'private' if your etcd cluster is within one region or 'public' if it spans regions or cloud providers.",
            "Default": "private",
            "AllowedValues": [
                "private",
                "public"
            ],
            "Type": "String"
        },
        "AllowSSHFrom": {
            "Description": "The net block (CIDR) that SSH is available to.",
            "Default": "0.0.0.0/0",
            "Type": "String"
        },
        "AllowPublicAccessFrom": {
            "Description": "The net block (CIDR) that Public Access is available to.",
            "Default": "0.0.0.0/0",
            "Type": "String"
        },
        "KeyPair": {
            "Description": "The name of an EC2 Key Pair to allow SSH access to the instance.",
            "Type": "String"
        }
    },
    "Resources": {
        "PublicELBSecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "Public ELB SecurityGroup",
                "SecurityGroupIngress": [
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "80",
                        "ToPort": "80",
                        "CidrIp": {
                            "Ref": "AllowPublicAccessFrom"
                        }
                    }
                ]
            },
            "Metadata": {
                "AWS::CloudFormation::Designer": {
                    "id": "6904be60-823e-41ce-818c-a0e293c81b31"
                }
            }
        },
        "PublicELB": {
            "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
            "Properties": {
                "AvailabilityZones": {
                    "Fn::GetAZs": ""
                },
                "Listeners": [
                    {
                        "LoadBalancerPort": "80",
                        "InstancePort": "80",
                        "Protocol": "HTTP"
                    }
                ],
                "HealthCheck": {
                    "Target": {
                        "Fn::Join": [
                            "",
                            [
                                "HTTP:",
                                "80",
                                "/"
                            ]
                        ]
                    },
                    "HealthyThreshold": "3",
                    "UnhealthyThreshold": "5",
                    "Interval": "30",
                    "Timeout": "5"
                },
                "SecurityGroups": [
                    {
                        "Fn::GetAtt": [
                            "PublicELBSecurityGroup",
                            "GroupId"
                        ]
                    }
                ]
            },
            "Metadata": {
                "AWS::CloudFormation::Designer": {
                    "id": "c9dea3d0-4b9b-4429-adfe-6037b39aed15"
                }
            }
        },
        "CoreOSSecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "CoreOS SecurityGroup",
                "SecurityGroupIngress": [
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "22",
                        "ToPort": "22",
                        "CidrIp": {
                            "Ref": "AllowSSHFrom"
                        }
                    }
                ]
            },
            "Metadata": {
                "AWS::CloudFormation::Designer": {
                    "id": "528e4b8e-9e1c-4748-a5cd-730783e0b039"
                }
            }
        },
        "Ingress4001": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "Properties": {
                "GroupName": {
                    "Ref": "CoreOSSecurityGroup"
                },
                "IpProtocol": "tcp",
                "FromPort": "4001",
                "ToPort": "4001",
                "SourceSecurityGroupId": {
                    "Fn::GetAtt": [
                        "CoreOSSecurityGroup",
                        "GroupId"
                    ]
                }
            }
        },
        "Ingress2379": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "Properties": {
                "GroupName": {
                    "Ref": "CoreOSSecurityGroup"
                },
                "IpProtocol": "tcp",
                "FromPort": "2379",
                "ToPort": "2379",
                "SourceSecurityGroupId": {
                    "Fn::GetAtt": [
                        "CoreOSSecurityGroup",
                        "GroupId"
                    ]
                }
            }
        },
        "Ingress2380": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "Properties": {
                "GroupName": {
                    "Ref": "CoreOSSecurityGroup"
                },
                "IpProtocol": "tcp",
                "FromPort": "2380",
                "ToPort": "2380",
                "SourceSecurityGroupId": {
                    "Fn::GetAtt": [
                        "CoreOSSecurityGroup",
                        "GroupId"
                    ]
                }
            }
        },
        "CoreOSServerAutoScale": {
            "Type": "AWS::AutoScaling::AutoScalingGroup",
            "Properties": {
                "AvailabilityZones": {
                    "Fn::GetAZs": ""
                },
                "LaunchConfigurationName": {
                    "Ref": "CoreOSServerLaunchConfig"
                },
                "MinSize": "3",
                "MaxSize": "12",
                "DesiredCapacity": {
                    "Ref": "ClusterSize"
                },
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": {
                            "Ref": "AWS::StackName"
                        },
                        "PropagateAtLaunch": true
                    }
                ],
                "LoadBalancerNames": [
                    {
                        "Ref": "PublicELB"
                    }
                ]
            },
            "Metadata": {
                "AWS::CloudFormation::Designer": {
                    "id": "244dc91b-d2d1-454c-9a51-71d0611e9e0e"
                }
            }
        },
        "CoreOSServerLaunchConfig": {
            "Type": "AWS::AutoScaling::LaunchConfiguration",
            "Properties": {
                "ImageId": {
                    "Fn::FindInMap": [
                        "RegionMap",
                        {
                            "Ref": "AWS::Region"
                        },
                        "AMI"
                    ]
                },
                "InstanceType": {
                    "Ref": "InstanceType"
                },
                "KeyName": {
                    "Ref": "KeyPair"
                },
                "SecurityGroups": [
                    {
                        "Ref": "CoreOSSecurityGroup"
                    }
                ],
                "UserData": {

          "Fn::Base64": {
            "Fn::Join": [
              "",
              [
                "#cloud-config\n\n",
                "coreos:\n",
                "  fleet:\n",
                "    public-ip: $public_ipv4",
                "\n",
                "    metadata: ",
                {
                  "Ref": "MachineMetadata"
                },
                "\n",
                "  etcd2:\n",
                "    discovery: ",
                {
                  "Ref": "DiscoveryURL"
                },
                "\n",
                "    advertise-client-urls: http://$",
                {
                  "Ref": "AdvertisedIPAddress"
                },
                "_ipv4:2379\n",
                "    initial-advertise-peer-urls: http://$",
                {
                  "Ref": "AdvertisedIPAddress"
                },
                "_ipv4:2380\n",
                "    listen-client-urls: http://0.0.0.0:2379,http://0.0.0.0:4001\n",
                "    listen-peer-urls: http://$",
                {
                  "Ref": "AdvertisedIPAddress"
                },
                "_ipv4:2380\n",
                "  units:\n",
                "    - name: etcd2.service\n",
                "      command: start\n",
                "    - name: fleet.service\n",
                "      command: start\n"
              ]
            ]
          }
		}
            },
            "Metadata": {
                "AWS::CloudFormation::Designer": {
                    "id": "d6d16533-0401-42e1-aa62-2b57c3d96820"
                }
            }
        }
    },
    "Metadata": {
        "AWS::CloudFormation::Designer": {
            "528e4b8e-9e1c-4748-a5cd-730783e0b039": {
                "size": {
                    "width": 60,
                    "height": 60
                },
                "position": {
                    "x": 180,
                    "y": 200
                },
                "z": 1,
                "embeds": []
            },
            "d6d16533-0401-42e1-aa62-2b57c3d96820": {
                "size": {
                    "width": 60,
                    "height": 60
                },
                "position": {
                    "x": 180,
                    "y": 90
                },
                "z": 1,
                "embeds": [],
                "ismemberof": [
                    "528e4b8e-9e1c-4748-a5cd-730783e0b039"
                ]
            },
            "244dc91b-d2d1-454c-9a51-71d0611e9e0e": {
                "size": {
                    "width": 60,
                    "height": 60
                },
                "position": {
                    "x": 20,
                    "y": 90
                },
                "z": 1,
                "embeds": [],
                "isconnectedto": [
                    "c9dea3d0-4b9b-4429-adfe-6037b39aed15"
                ],
                "isassociatedwith": [
                    "d6d16533-0401-42e1-aa62-2b57c3d96820"
                ]
            },
            "c9dea3d0-4b9b-4429-adfe-6037b39aed15": {
                "size": {
                    "width": 60,
                    "height": 60
                },
                "position": {
                    "x": -120,
                    "y": 90
                },
                "z": 0,
                "embeds": [],
                "ismemberof": [
                    "6904be60-823e-41ce-818c-a0e293c81b31"
                ]
            },
            "dcdc1761-9db5-4058-a65d-dd003ace5a4b": {
                "source": {
                    "id": "244dc91b-d2d1-454c-9a51-71d0611e9e0e"
                },
                "target": {
                    "id": "c9dea3d0-4b9b-4429-adfe-6037b39aed15"
                },
                "z": 2
            },
            "6904be60-823e-41ce-818c-a0e293c81b31": {
                "size": {
                    "width": 60,
                    "height": 60
                },
                "position": {
                    "x": -120,
                    "y": 200
                },
                "z": 0,
                "embeds": []
            },
            "11839107-4142-4cd7-b36a-49ea8b8c96a1": {
                "source": {
                    "id": "c9dea3d0-4b9b-4429-adfe-6037b39aed15"
                },
                "target": {
                    "id": "6904be60-823e-41ce-818c-a0e293c81b31"
                },
                "z": 2
            }
        }
    }
}
